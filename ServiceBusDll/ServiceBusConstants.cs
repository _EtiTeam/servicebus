﻿namespace ServiceBusDLL
{
    public static class ServiceBusConstants
    {
        // services name
        public const string SALES_SERVICE_NAME = "SalesService";

        public const string ACCOUNTANCY_SERVICE_NAME = "AccountancyService";

        public const string WAREHOUSE_SERVICE_NAME = "WarehouseService";

        public const string SPEDITION_SERVICE_NAME = "SpeditionService";

        public const string TEST_SERVICE_NAME = "TestService";
    }
}
