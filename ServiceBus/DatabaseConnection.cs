﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ModelDLL;

namespace ServiceBus
{
    
    public class DatabaseConnection : ModelDLL.IDatabaseConnection
    {
        public SqlConnection SqlConnection => getConnection();

        private static SqlConnection getConnection()
        {
            SqlConnection c = new SqlConnection();
            // Sales
            //c.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;Database=SalesDatabase;Trusted_Connection=yes;"; // from Properties/Settings.settings
            // Accountancy
            c.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;Database=AccountancyDatabase;Trusted_Connection=yes;"; // from Properties/Settings.settings
            return c;
        }
    }
}