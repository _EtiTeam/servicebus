﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NServiceBus;
using NServiceBus.Logging;
using ServiceBusDLL;
using ServiceBus.Utils;

namespace ServiceBus
{
    class BusStarter
    {
        static ILog log = LogManager.GetLogger<BusStarter>();

        static void Main(string[] args)
        {
            AsyncMain().GetAwaiter().GetResult();
        }

        static async Task AsyncMain()
        {
            var endpointConfiguration = ServiceBusConfigurator.getConfiguration(ServiceBusConstants.TEST_SERVICE_NAME);

            var endpointInstance = await Endpoint.Start(endpointConfiguration);

            await RunLoop(endpointInstance);

            await endpointInstance.Stop();
        }

        static async Task RunLoop(IEndpointInstance endpointInstance)
        {
            while (true)
            {
                log.Info(
                    "\n\tPress 'R' to do request/response, " +
                    "\n\t'O' to do startOrder(id=1), " +
                    "\n\t'P' to do orderPaid(id=1), " +
                    "\n\t'Q' to quit.");
                var key = Console.ReadKey();
                Console.WriteLine();

                switch (key.Key)
                {
                    case ConsoleKey.R:
                        var command = new GetObjects(1, typeof(ModelDLL.User));
                        string destination = ServiceBusConstants.TEST_SERVICE_NAME;

                        var a = await endpointInstance.Request<GetUsersResponse>(destination, command);
                        IList<ModelDLL.User> users = a.databaseObjects();
                        ModelDLL.User user = users.First();
                        break;

                    case ConsoleKey.O:
                        var command2 = new StartOrder { OrderId = 1 };
                        await endpointInstance.Send(ServiceBusConstants.SALES_SERVICE_NAME, command2);
                        log.Info($"Sended startOrder: { command2.Id }");
                        break;

                    case ConsoleKey.P:
                        var command3 = new OrderPaid { OrderId = 1 };
                        await endpointInstance.Send(ServiceBusConstants.ACCOUNTANCY_SERVICE_NAME, command3);
                        log.Info($"Sended OrderPaid: { command3.OrderId }");
                        break;

                    case ConsoleKey.Q:
                        return;

                    default:
                        log.Info("Unknown input. Please try again.");
                        break;
                }
            }
        }
    }
}
