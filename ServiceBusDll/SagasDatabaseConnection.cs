﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBusDLL
{
    public class SagasDatabaseConnection : ModelDLL.IDatabaseConnection
    {
        public SqlConnection SqlConnection => new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Database=NServiceBusDatabase;Trusted_Connection=yes;");
    }
}
