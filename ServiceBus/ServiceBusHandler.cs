﻿using NServiceBus;
using NServiceBus.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using ServiceBusMessagesDLL;
using System;

namespace ServiceBus
{
    public class ServiceBusHandler :
        IHandleMessages<GetObjects>,
        IHandleMessages<PlacedOrder>
    {
        static ILog log = LogManager.GetLogger<ServiceBusHandler>();

        public Task Handle(GetObjects message, IMessageHandlerContext context)
        {
            log.Info($"GetObject handler");
            if (typeof(ModelDLL.User).FullName.Equals(message.databaseObjectTypeToGet))
                return context.Reply(new GetUsersResponse { serializedDatabaseObjects = fetchJson<ModelDLL.User>(message) });

            // TODO add exception #77
            return Task.CompletedTask; // if not found
        }

        private static string fetchJson<T>(GetObjects message)
            where T : class, ModelDLL.IDatabaseObject, new()
        {
            IList<T> list = new List<T>();
            foreach (int id in message.databaseObjectIds)
                list.Add(ModelDLL.ModelOperations.Select<T>(id, new DatabaseConnection()));
            return ModelDLL.ModelOperations.toJson(list);
        }

        public Task Handle(PlacedOrder message, IMessageHandlerContext context)
        {
            log.Info("ok");
            throw new Exception("bum");
            return Task.CompletedTask;
        }
    }
}
