﻿using NServiceBus;
using NServiceBus.Logging;
using ServiceBusDLL;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using System;

namespace ServiceBus
{
    class UniversalMessagesHandler :
        IHandleMessages<GetObjects>,
        IHandleMessages<GetUsersResponse> // TODO to remove
    {
        static ILog log = LogManager.GetLogger<UniversalMessagesHandler>();

        public Task Handle(GetObjects message, IMessageHandlerContext context)
        {
            log.Info($"Fetched GetObjects { message.databaseObjectTypeToGet } " +
                $"ids: { serializeList(message.databaseObjectIds) }");

            if (typeof(ModelDLL.User).FullName.Equals(message.databaseObjectTypeToGet))
                return context.Reply(new GetUsersResponse { serializedDatabaseObjects = fetchResponseJson<ModelDLL.User>(message) });

            // TODO add exception #77 
            return Task.CompletedTask; // if not found
        }

        public Task Handle(GetUsersResponse message, IMessageHandlerContext context)
        {
            return Task.CompletedTask;
        }

        private static string fetchResponseJson<T>(GetObjects message)
            where T : class, ModelDLL.IDatabaseObject, new()
        {
            IList<T> list = new List<T>();
            foreach (int id in message.databaseObjectIds)
                list.Add(ModelDLL.ModelOperations.Select<T>(id, new DatabaseConnection()));
            return ModelDLL.ModelOperations.toJson(list);
        }

        private static string serializeList<T>(List<T> list)
        {
            StringBuilder b = new StringBuilder();
            b.Append("[");
            list.ForEach(i => b.Append(i.ToString()).Append(","));
            b.Remove(b.Length - 1, 1);
            b.Append("]");
            return b.ToString();
        }
    }
}
