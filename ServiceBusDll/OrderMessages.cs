﻿using System;
using NServiceBus;

namespace ServiceBusDLL
{
    public interface MessageBase : IMessage
    {
        Guid Id { get; set; }
    }

    public abstract class EventBase : IEvent, MessageBase
    {
        public Guid Id { get; set; }
    }

    public abstract class CommandBase : ICommand, MessageBase
    {
        public Guid Id { get; set; }
    }

    public class RetryConfirmationOrderTimeout : CommandBase
    {
        public Guid RetryReservationId { get; set; }
    }

    // buissines logic - order
    public class StartOrder : CommandBase {
        
        public StartOrder()
        {
            Id = Guid.NewGuid();
        }

        public int OrderId { get; set; }
    }

    public class CheckOrderPayment : CommandBase
    {
        public int OrderId { get; set; }
    }

    // local message in accountancy server
    public class OrderPaid : ICommand
    {
        public int OrderId { get; set; }
    }

    public class CheckOrderPaymentResponse : CommandBase { }

    public class OrderCommitted : EventBase
    {
        public Guid OrderSagaId { get; set; }
        public int BookId { get; set; }
    }

    public class OrderItemReserved : CommandBase
    {
        public Guid ReservationId { get; set; }
    }

    public class OrderItemNotReserved : CommandBase
    {
        public Guid ConfirmationId { get; set; }
    }

    public class OrderProviderIsReady : CommandBase
    {
        public Guid ReservationId { get; set; }
    }

    public class OrderProviderIsNotReady : CommandBase
    {
        public Guid ConfirmationId { get; set; }
    }

    public class OrderReservationRollback : EventBase { }

    public class ProviderIsReadyTimeout : CommandBase { }

    public class OrderFinished : EventBase { }
}
