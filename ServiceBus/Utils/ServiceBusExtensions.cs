﻿using NServiceBus;
using NServiceBus.Callbacks;
using System.Threading.Tasks;

namespace ServiceBus.Utils
{
    public static class ServiceBusExtensions
    {
        public static Task<T> Request<T>(this IEndpointInstance context, string destination, object requestObject)
        {
            return context.Request<T>(requestObject, getSendOptions(destination));
        }

        private static SendOptions getSendOptions(string destination)
        {
            var sendOptions = new SendOptions();
            sendOptions.SetDestination(destination);
            return sendOptions;
        }
    }
}
