﻿using NServiceBus;
using NServiceBus.Logging;
using ServiceBusDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBus
{
    class WarehouseMessageHandler
        : IHandleMessages<OrderCommitted>
    {
        static ILog log = LogManager.GetLogger<WarehouseMessageHandler>();

        public Task Handle(OrderCommitted message, IMessageHandlerContext context)
        {
            // empty
            return Task.CompletedTask;
        }
    }
}
