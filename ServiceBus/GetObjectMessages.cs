﻿using System;
using System.Collections.Generic;
using NServiceBus;

namespace ServiceBusDLL
{
    public class GetObjects : ICommand
    {
        // if you would like to get only one object
        public GetObjects(int id, Type type)
        {
            databaseObjectIds = new List<int> { id };
            setDatabaseObjectToGet(type);
        }

        public GetObjects(List<int> ids, Type type)
        {
            databaseObjectIds = ids;
            setDatabaseObjectToGet(type);
        }

        public List<int> databaseObjectIds { get; set; }
        public string databaseObjectTypeToGet { get; set; }

        public Type getDatabaseObjectToGet()
            => Type.GetType(databaseObjectTypeToGet);
        public void setDatabaseObjectToGet(Type type)
            => databaseObjectTypeToGet = type.FullName;
    }

    // specyfic response for all of model objects
    public class GetUsersResponse : GetObjectsResponse<ModelDLL.User> { }
    // TODO #75


    public abstract class GetObjectsResponse<T> : IMessage
        where T : class, ModelDLL.IDatabaseObject
    {
        public string serializedDatabaseObjects { get; set; }
        public IList<T> databaseObjects()
            => ModelDLL.ModelOperations.GetFromJson<T>(serializedDatabaseObjects);
    }
}
