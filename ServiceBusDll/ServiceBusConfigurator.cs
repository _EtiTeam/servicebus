﻿using NServiceBus;
using System;
using NServiceBus.Logging;
using NServiceBus.Persistence.Sql;
using System.Data.SqlClient;

namespace ServiceBusDLL
{
    public class ServiceBusConfigurator
    {
        static ILog log = LogManager.GetLogger<ServiceBusConfigurator>();

        public static EndpointConfiguration getConfiguration(string endpointName)
        {
            //Console.Title = endpointName; // it's optional

            var endpointConfiguration = new EndpointConfiguration(endpointName); // name of endpoint

            // settings required for request method
            var discriminator = endpointName;
            endpointConfiguration.MakeInstanceUniquelyAddressable(discriminator); // TODO #73
            endpointConfiguration.EnableCallbacks();
            endpointConfiguration.EnableInstallers(); // in order to create required queues
            //endpointConfiguration.SendFailedMessagesTo("targetErrorQueue");

            endpointConfiguration.DefineCriticalErrorAction(
                onCriticalError: async context => // reaction on critical error
                {
                    // Log the critical error
                    log.Fatal($"CRITICAL: {context.Error}", context.Exception);

                    await context.Stop()
                        .ConfigureAwait(false);

                    // Kill the process on a critical error
                    var output = $"NServiceBus critical error:\n{context.Error}\nShutting down.";
                    Environment.FailFast(output, context.Exception);
                });

            // setting logs level
            //var defaultFactory = LogManager.Use<DefaultFactory>();
            //defaultFactory.Level(LogLevel.Warn);

            // setting queue
            //var transport = endpointConfiguration.UseTransport<LearningTransport>();
            //transport.StorageDirectory("g:\\adasdj");    // TODO to set
            //transport.NoPayloadSizeRestriction();       // unlimited space
            var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
            var delayedDelivery = transport.DelayedDelivery();
            delayedDelivery.DisableTimeoutManager();    // because RabbitMq suports it natively

            //transport.Transactions(TransportTransactionMode.TransactionScope);                // TODO #78
            //transport.TransactionScopeOptions(isolationLevel: IsolationLevel.RepeatableRead); // TODO #78
            transport.Transactions(TransportTransactionMode.ReceiveOnly);

            var persistence = endpointConfiguration.UsePersistence<SqlPersistence>();
            //persistence.SagaStorageDirectory("g:\\adasd");    // TODO to set
            persistence.SqlVariant(SqlVariant.MsSqlServer);
            persistence.ConnectionBuilder(connectionBuilder: () => new SagasDatabaseConnection().SqlConnection);
            
            // setting default routing
            // var routing = transport.Routing();
            //routing.RouteToEndpoint(typeof(ServiceBusMessagesDLL.GetObjects), ServiceBusConstants.SALES_SERVICE_NAME);
            return endpointConfiguration;
        }
    }
}
